#!/bin/bash

default_make="make"
default_makeflags="-j $(nproc)"

MAKE="${MAKE:-$default_make}"
MAKEFLAGS="${MAKEFLAGS:-$default_makeflags}"

cd riscv-gcc
export CC=/usr/lib/ccache/gcc
export CXX=/usr/lib/ccache/g++
./configure --enable-multilib
$MAKE $MAKEFLAGS
