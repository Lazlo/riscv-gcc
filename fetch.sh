#!/bin/bash

d=riscv-gcc
if [ -d $d ]; then
	exit 0
fi
git clone --depth 1 https://github.com/riscv/riscv-gcc.git
