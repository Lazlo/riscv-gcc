#!/bin/bash

set -e
set -u

pkgs=""
pkgs="$pkgs make"
pkgs="$pkgs gcc"
pkgs="$pkgs libgmp-dev"		# 4.2+
pkgs="$pkgs libmpfr-dev"	# 2.4.0+
pkgs="$pkgs libmpc-dev"		# 0.8.0+
pkgs="$pkgs ccache"

apt-get -q install -y $pkgs
